import java.util.HashMap;
import java.util.Map;

//Good morning! Here's your coding interview problem for today.
//
//This problem was recently asked to me by SonarSource.
//
//Given two strings, check if one is an anagram of the other

//Bonus: What's the time complexity of your solution?

public class Problem_0 {
	

	public static void main(String[] args) {
		
		String a = "rat";
		String b = "tar";
		String c = "eleven plus two";
		String d = "twelve plus one";
		String e = "slot machines";
		String f = "cash lost in you";

		isAnagram(a,b);
		isAnagram(c,d);
		isAnagram(e,f);
	}

	//This solution is O(2N) -> O(N)
	private static void isAnagram(String word1, String word2) {
		Map<String, Integer> map = new HashMap<>(); 
		
		for(int i = 0; i < word1.length(); i++) {
			String current = String.valueOf(word1.toCharArray()[i]);
			int count = map.get(current) == null ? 1 : map.get(current)+1;
			map.put(current, count);
		}
		
		for(int j = 0; j < word2.length(); j++) {
			String current = String.valueOf(word2.toCharArray()[j]);
			if(map.get(current) == null){
				break;
			}
			else {
				int count = map.get(current)-1;
				if(count == 0) {
					map.remove(current);
				}
				else {
					map.put(current, count);
				}
			}
		}
		if(map.isEmpty()) {
			System.out.println("\"" + word2 + "\" is an anagram of \"" + word1 + "\"");
		}
		else {
			System.out.println("\"" + word2 + "\" is not an anagram of \"" + word1 + "\"");
		}
	}
}
