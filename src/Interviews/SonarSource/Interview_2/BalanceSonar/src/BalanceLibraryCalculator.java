package Interviews.SonarSource.Interview_2.BalanceSonar.src;

import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

import Interviews.SonarSource.Interview_2.BalanceSonar.src.executors.CommandExecutor;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.executors.ExpenseCommandExecutor;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.executors.IncomeCommandExecutor;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.executors.PrintCommandExecutor;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Command;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Database;

public class BalanceLibraryCalculator {
	static Database database = new Database();
	static ExpenseCommandExecutor expenseCommand = new ExpenseCommandExecutor(database);
	static IncomeCommandExecutor incomeCommand = new IncomeCommandExecutor(database);
	static PrintCommandExecutor printCommand = new PrintCommandExecutor(database);

	public static void main(String[] args) {
		List<CommandExecutor> commandExecutorList = new ArrayList<>();		
		commandExecutorList.addAll(Arrays.asList(
				expenseCommand,
				incomeCommand,
				printCommand));
		
        Scanner scanner = new Scanner(System.in);
        System.out.println(">>");
        
        String s = scanner.nextLine();
        while(!s.equals("STOP")) {
        	Command command = new Command();
        	List<String> commandParams = new ArrayList<>();
        	CommandRunner runner = new CommandRunner(commandExecutorList);
        	String[] commandArray = s.split(" ");        	
        	command.setName(commandArray[0]);
        	
        	for(int i = 1; i<commandArray.length; i++) {
        		commandParams.add(commandArray[i]);
        	}
        	command.setParams(commandParams);
            runner.runCommand(command);
            s = scanner.nextLine();
        }
        scanner.close();
	}
}
