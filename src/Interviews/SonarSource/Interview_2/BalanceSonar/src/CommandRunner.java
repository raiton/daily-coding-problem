package Interviews.SonarSource.Interview_2.BalanceSonar.src;

import java.util.List;

import Interviews.SonarSource.Interview_2.BalanceSonar.src.executors.CommandExecutor;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Command;

public class CommandRunner {
	
	List<CommandExecutor> commandExecutors;

	public CommandRunner(List<CommandExecutor> commandExecutors) {
		this.commandExecutors = commandExecutors;
	}

	public String runCommand(Command command) {
		for (CommandExecutor commandExecutor: commandExecutors) {
			if (commandExecutor.isApplicable(command)) {
				return commandExecutor.execute(command);
			}
		}

		return "Invalid Command";
	}
}
