package Interviews.SonarSource.Interview_2.BalanceSonar.src.executors;

import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Command;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Database;

abstract public class CommandExecutor {
	
	protected Database database;

	public CommandExecutor(Database database) {
		this.database = database;
	}
	
	public String execute(final Command command) {
		return executeValidCommand(command);
	}
	
	public abstract Boolean isApplicable(final Command command);
	
	protected abstract String  executeValidCommand(final Command command);
}
