package Interviews.SonarSource.Interview_2.BalanceSonar.src.executors;

import java.time.LocalDate;

import Interviews.SonarSource.Interview_2.BalanceSonar.src.enums.Type;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.objects.IncomeExpense;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Command;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Database;

public class ExpenseCommandExecutor extends CommandExecutor{
	
	static final String EXPENSE = "EXPENSE";

	public ExpenseCommandExecutor(Database database) {
		super(database);
	}
	
	public Boolean isApplicable(Command command) {
		return command.getName().equals(EXPENSE);
	}

	@Override
	protected String executeValidCommand(Command command) {
		IncomeExpense incomeExpense = new IncomeExpense(LocalDate.parse(command.getParams().get(0).replaceAll("/", "-")), command.getParams().get(1), Double.valueOf(command.getParams().get(2)), Type.EXPENSE);
		database.saveIncomeExpense(incomeExpense);
		return "Income Saved";
	}
}

