package Interviews.SonarSource.Interview_2.BalanceSonar.src.executors;

import java.time.LocalDate;

import Interviews.SonarSource.Interview_2.BalanceSonar.src.enums.Type;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.objects.IncomeExpense;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Command;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Database;

public class IncomeCommandExecutor extends CommandExecutor{
	
	static final String INCOME = "INCOME";

	public IncomeCommandExecutor(Database database) {
		super(database);
	}
	
	public Boolean isApplicable(Command command) {
		return command.getName().equals(INCOME);
	}

	@Override
	protected String executeValidCommand(Command command) {
		IncomeExpense incomeExpense = new IncomeExpense(LocalDate.parse(command.getParams().get(0).replaceAll("/", "-")), command.getParams().get(1), Double.valueOf(command.getParams().get(2)), Type.INCOME);
		database.saveIncomeExpense(incomeExpense);
		return "Income Saved";
	}
}
