package Interviews.SonarSource.Interview_2.BalanceSonar.src.executors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Interviews.SonarSource.Interview_2.BalanceSonar.src.CommandRunner;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Command;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Database;

public class PrintCommandExecutor extends CommandExecutor{
	
	static final String PRINT = "PRINT";
	static PrintDayCommandExecutor printDayExecutor;
	static PrintMonthCommandExecutor printMonthExecutor;
	static PrintYearCommandExecutor printYearExecutor;

	public PrintCommandExecutor(Database database) {
		super(database);
		printDayExecutor = new PrintDayCommandExecutor(database);
		printMonthExecutor = new PrintMonthCommandExecutor(database);
		printYearExecutor = new PrintYearCommandExecutor(database);
	}
	
	public Boolean isApplicable(Command command) {
		return command.getName().equals(PRINT);
	}

	@Override
	protected String executeValidCommand(Command command) {
		
		List<CommandExecutor> commandExecutorList = new ArrayList<>();
		commandExecutorList.addAll(Arrays.asList(
				printDayExecutor,
				printMonthExecutor,
				printYearExecutor));
		
		List<String> oldParams = command.getParams();
		
		command.setName(oldParams.get(0));
		oldParams.remove(0);
		
    	CommandRunner runner = new CommandRunner(commandExecutorList);
        return runner.runCommand(command);
	}
}
