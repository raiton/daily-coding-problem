package Interviews.SonarSource.Interview_2.BalanceSonar.src.executors;

import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Command;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Database;

public class PrintDayCommandExecutor extends CommandExecutor{
	
	static final String DAY = "DAY";

	public PrintDayCommandExecutor(Database database) {
		super(database);
	}
	
	public Boolean isApplicable(Command command) {
		return command.getName().equals(DAY);
	}

	@Override
	protected String executeValidCommand(Command command) {
		String date = command.getParams().get(0);
		String value = String.valueOf(database.getDayBalance(date));
		System.out.println(value);
		return value;
	}
}

