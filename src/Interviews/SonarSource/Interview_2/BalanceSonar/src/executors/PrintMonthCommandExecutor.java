package Interviews.SonarSource.Interview_2.BalanceSonar.src.executors;

import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Command;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Database;

public class PrintMonthCommandExecutor extends CommandExecutor{
	
	static final String MONTH = "MONTH";

	public PrintMonthCommandExecutor(Database database) {
		super(database);
	}
	
	public Boolean isApplicable(Command command) {
		return command.getName().equals(MONTH);
	}

	@Override
	protected String executeValidCommand(Command command) {
		String date = command.getParams().get(0);
		String value = String.valueOf(database.getMonthBalance(date));
		System.out.println(value);
		return value;
	}
}
