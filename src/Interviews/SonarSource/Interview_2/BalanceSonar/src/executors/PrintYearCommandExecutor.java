package Interviews.SonarSource.Interview_2.BalanceSonar.src.executors;

import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Command;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.other.Database;

public class PrintYearCommandExecutor extends CommandExecutor{
	
	static final String YEAR = "YEAR";

	public PrintYearCommandExecutor(Database database) {
		super(database);
	}
	
	public Boolean isApplicable(Command command) {
		return command.getName().equals(YEAR);
	}

	@Override
	protected String executeValidCommand(Command command) {
		String date = command.getParams().get(0);
		String value = String.valueOf(database.getYearBalance(date));
		System.out.println(value);
		return value;
	}
}
