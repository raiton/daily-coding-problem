package Interviews.SonarSource.Interview_2.BalanceSonar.src.objects;

import java.time.LocalDate;

import Interviews.SonarSource.Interview_2.BalanceSonar.src.enums.Type;


public class IncomeExpense {
	Type type;
	LocalDate date;
	String description;
	double amount;
	
	public IncomeExpense(LocalDate date, String description, double amount, Type type){
		this.date = date;
		this.description = description;
		this.amount = amount;
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
}
