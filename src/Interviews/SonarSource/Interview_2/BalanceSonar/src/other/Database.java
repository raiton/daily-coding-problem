package Interviews.SonarSource.Interview_2.BalanceSonar.src.other;

import java.util.ArrayList;
import java.util.List;

import Interviews.SonarSource.Interview_2.BalanceSonar.src.enums.Type;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.objects.IncomeExpense;

public class Database {
	static List<IncomeExpense> dataList = new ArrayList<>();
	
	private static List<IncomeExpense> getList() {
		return dataList;
	}

	public void saveIncomeExpense(IncomeExpense incomeExpense) {
		dataList.add(incomeExpense);
	}

	public double getDayBalance(String inputDate) {
		double balance = 0;
		for(IncomeExpense value: getList()) {
			String dayValue = value.getDate().getDayOfMonth() < 10 ? 0 + String.valueOf(value.getDate().getDayOfMonth()) : String.valueOf(value.getDate().getDayOfMonth());;
			String monthValue = value.getDate().getMonthValue() < 10 ? 0 + String.valueOf(value.getDate().getMonthValue()) : String.valueOf(value.getDate().getMonthValue());
			if(String.valueOf(value.getDate().getYear()).equals(inputDate.split("/")[0]) && monthValue.equals(inputDate.split("/")[1]) && dayValue.equals(inputDate.split("/")[2])) {
				if(value.getType().equals(Type.EXPENSE)) {
					balance-= value.getAmount();
				}	
				else if(value.getType().equals(Type.INCOME)) {
					balance+= value.getAmount();
				}
			}
		}
		return balance;
	}
	
	public double getMonthBalance(String inputDate) {
		double balance = 0;
		for(IncomeExpense value: getList()) {
			String monthValue = value.getDate().getMonthValue() < 10 ? 0 + String.valueOf(value.getDate().getMonthValue()) : String.valueOf(value.getDate().getMonthValue());
			if(String.valueOf(value.getDate().getYear()).equals(inputDate.split("/")[0]) && monthValue.equals(inputDate.split("/")[1])) {
				if(value.getType().equals(Type.EXPENSE)) {
					balance-= value.getAmount();
				}	
				else if(value.getType().equals(Type.INCOME)) {
					balance+= value.getAmount();
				}
			}
		}
		return balance;		
	}

	public double getYearBalance(String inputDate) {
		double balance = 0;
		for(IncomeExpense value: getList()) {
			if(String.valueOf(value.getDate().getYear()).equals(inputDate)) {
				if(value.getType().equals(Type.EXPENSE)) {
					balance-= value.getAmount();
				}	
				else if(value.getType().equals(Type.INCOME)) {
					balance+= value.getAmount();
				}
			}
		}
		return balance;	
	}

}
