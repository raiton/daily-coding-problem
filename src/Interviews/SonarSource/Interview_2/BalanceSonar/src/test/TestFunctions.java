package Interviews.SonarSource.Interview_2.BalanceSonar.src.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import Interviews.SonarSource.Interview_2.BalanceSonar.src.dataManager.GetData;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.dataManager.SaveData;
import Interviews.SonarSource.Interview_2.BalanceSonar.src.objects.IncomeExpense;


class TestFunctions {

	
//	@Test
//	void test() {
//		tests.
//		test1();
//		test2();
//		test3();
//		assertAll(test1());
//		assertAll();
//	}

	@Test
	private void test1() {
		List<IncomeExpense> list = new ArrayList<>();
		String input1 = "INCOME 2020/01/01 gift 500";
		SaveData.saveIncome(input1, list);
		String input2 = "PRINT YEAR 2021";
	    double expectedOutput = 0;
	    double actualOutput = GetData.printYear(input2, list);
		assertEquals(expectedOutput, actualOutput, "");
	}
	
	@Test
	private void test2() {
		List<IncomeExpense> list = new ArrayList<>();
		String input1 = "INCOME 2020/01/01 gift 500";
		SaveData.saveIncome(input1, list);
		String input2 = "PRINT YEAR 2020";
	    double expectedOutput = 500;
	    double actualOutput = GetData.printYear(input2, list);
		assertEquals(expectedOutput, actualOutput, "");
	}
	
	@Test
	private void test3() {
		List<IncomeExpense> list = new ArrayList<>();
		String input1 = "INCOME 2020/01/01 gift 500";
		String input2 = "EXPENSE 2020/10/12 coffee 5";
		String input3 = "EXPENSE 2020/10/11 lunch 25";
		SaveData.saveIncome(input1, list);
		String input4 = "PRINT YEAR 2020";
	    double expectedOutput = 500;
	    double actualOutput = GetData.printYear(input4, list);
		assertEquals(expectedOutput, actualOutput, "");
	}

}


//>>INCOME 2020/01/01 gift 500
//>>PRINT YEAR 2021 
//0
//>>PRINT YEAR 2020 
//500
//>>EXPENSE 2020/10/12 coffee 5
//>>EXPENSE 2020/10/11 lunch 25
//>>PRINT YEAR 2020 
//470
//>>INCOME 2020/10/25 stock-dividend 40
//>>PRINT MONTH 2020/11 
//0
//>>PRINT MONTH 2020/10 
//10
//>>PRINT DAY 2020/10/11 
//-25