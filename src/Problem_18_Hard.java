//Good morning! Here's your coding interview problem for today.
//
//This problem was asked by Google.
//
//Given an array of integers and a number k, where 1 <= k <= length of the array, compute the maximum values of each subarray of length k.
//
//For example, given array = [10, 5, 2, 7, 8, 7] and k = 3, we should get: [10, 7, 8, 8], since:
//
//10 = max(10, 5, 2)
//7 = max(5, 2, 7)
//8 = max(2, 7, 8)
//8 = max(7, 8, 7)
//Bonus: Do this in O(n) time and O(k) space. You can modify the input array in-place and you do not need to store the results. You can simply print them out as you compute them.

public class Problem_18_Hard {

	public static void main(String[] args) {
		int[] list = {10, 5, 2, 7, 8, 7};
		int k = 3;
		
		
		getMaxArray(list, k);
	}

	//NOTE: this is an O(kn) solution

	
	
	/**
	 * @param list - input array
	 * @param k - size of each sub array
	 * takes an array and a number k where 1 <= k <= length of the array, compute the maximum values of each subarray of length k.
	 * Complexity - O(kn) 
	 */
	private static void getMaxArray(int[] list, int k) {
		for(int i = 0; i < list.length - 2; i++) {
			System.out.print(Math.max(list[i], Math.max(list[i+1], list [i+2])) + " ");			
		}
	}
}
