import java.util.HashMap;
import java.util.Map;

//Good morning! Here's your coding interview problem for today.
//
//This problem was recently asked by Google.
//
//Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
//
//For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
//
//Bonus: Can you do this in one pass?

public class Problem_1_Easy {
//	static List<Integer> list = new ArrayList<>();
	
	public static void main(String[] args) {
		int[] list = {10, 15, 3, 7};
		int k = 17;
		
		System.out.println("k = " + k + " and list : {10, 15, 3, 7}");
		System.out.println();
		System.out.println("Multiple pass:");
		System.out.println(multiplePass(list, k) ? "Yes" : "No");
		System.out.println();
		System.out.println("Single pass:");
		System.out.println(singlePass(list, k) ? "Yes" : "No");
	}

	private static boolean multiplePass(int[] list, int k) {
		for(int i = 0; i<list.length; i++) {
			for(int j = i+1; j<list.length; j++) {
				int atual = list[i] + list[j];
				System.out.println(list[i] + " + " + list[j] + " = " +  atual);
				if(atual == k)
				{
					return true;
				}
			}
		}
		return false;
	}
	
	private static boolean singlePass(int[] list, int k) {
		Map<Integer, Integer> map = new HashMap<>();
		for(int i = 0; i<list.length; i++) {
			int atual = list[i];
			System.out.print("Reading " + atual +" and checking for pair....");
			int oposite = k - atual;
			if (map.get(oposite) != null){
				System.out.println("Pair found! " + atual + " + " + map.get(oposite) + " = " +  k);
				return true;
			}else {
				System.out.println("No pair found, saving " + atual + " in memory");
				map.put(atual, atual);
			}
		}
		return false;
	}
	
}

