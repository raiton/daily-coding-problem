import java.util.ArrayList;
import java.util.List;

//Good morning! Here's your coding interview problem for today.
//
//This problem was asked by Uber.
//
//Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.
//
//For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
//
//Follow-up: what if you can't use division?


public class Problem_2_Hard {
	
	public static void main(String[] args) {
		int[] list = {1, 2, 3, 4, 5};
		System.out.println(calculateMultiplicationArray(list));
		int[] list2 = {10, 3, 5, 6, 2};
		System.out.println(calculateMultiplicationArray(list2));
	}

	private static String calculateMultiplicationArray(int[] list) {
		List<Integer> newList = new ArrayList<>();
		for(int i = 0; i < list.length; i++) {
			int product = 0;
			for(int j = 0; j < list.length; j++) {
				if(j != i) {
					if(j != i && j == 0 ) {
						product = list[j];
					}else {
						product *= list[j];
					}
				}else if(j == 0) {
					product = 1;				
				}
				
			}
			newList.add(product);
		}
		return newList.toString();
	}
}
