import java.util.HashMap;

//
//Good morning! Here's your coding interview problem for today.
//
//This problem was asked by Stripe.
//
//Given an array of integers, find the first missing positive integer in linear time and constant space. In other words, find the lowest positive integer that does not exist in the array. The array can contain duplicates and negative numbers as well.
//
//For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.
//
//You can modify the input array in-place.


public class Problem_4_Hard {

	public static void main(String[] args) {
		
		int[] list = {3, 4, -1, 1};
		findLowestPositive(list);
		int[] list2 = {1, 2, 0};
		findLowestPositive(list2);
	}

	private static void findLowestPositive(int[] list) {
		HashMap<Integer, Integer> map = new HashMap<>();
		for(int i = 0; i < list.length; i++) {
			int current = list[i];
			map.put(current, current);
		}
		
		for(int i= 1; i <= list.length; i++) {
			if(map.get(i) == null) {
				System.out.println("The lowest positive integer that does not exist in the given list is: " + i);
			}
		}
	}
	
	//Another solution would be to initialize a variable 1, and transverse the array changing the variable to x+1 everytime x was equal to the current saved number. This would decrease the memory use
}
